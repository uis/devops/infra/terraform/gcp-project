# Google Cloud Platform Project

This module creates a project on GCP (Google Cloud Platform), enables services
in it and, optionally, configures additional project editors. A service
account is created with owner rights on the project.

This module conforms to the [terraform standard module
structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).

The email and credentials for the service account are available from the project
[outputs](outputs.tf). Configuration variables are documented in [variables.tf](variables.tf).

This module will use the ``google`` and ``google-beta`` providers to create resources.
These are usually configured to only have access within the project created by
this module and so one usually uses provider
[aliases](https://www.terraform.io/docs/configuration/providers.html#multiple-provider-instances)
to provided a specialised "admin" provider to the module.

## Usage Examples

The [examples](examples/) directory contains examples of use. A basic usage
example is available in [examples/root-example](examples/root-example/). All
examples use the Google default application credentials to attempt project
creation. It is recommended that one create a service account, give it
permissions to create projects in the target folder and generate JSON
credentials. The ``GOOGLE_APPLICATION_CREDENTIALS`` environment variable may
then be set to the location of those credentials as usual.

## Using a Specific Version

If you require a specific version (branch or tag) of this repository then
state it when specifying the module source. For example, to use the
last version to support Terraform v0.11, use the `v1.1.0` tag:
```yaml
  source = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-project.git?ref=v1.1.0"
```

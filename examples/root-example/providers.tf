# Project creation is a privileged operation and is best left to a dedicated
# service account. This sets up "google.admin" and "google-beta.admin" providers
# configured with elevated permissions. Google default application credentials
# are used and can be specified by setting the GOOGLE_APPLICATION_CREDENTIALS
# environment variable to a path to JSON service account credentials.
#
# Both the google and google-beta provider are configured identically.
provider "google" {
  version = "~> 3.13"
  alias   = "admin"
}

provider "google-beta" {
  version = "~> 3.13"
  alias   = "admin"
}

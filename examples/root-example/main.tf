module "project" {
  source = "git::ssh://git@gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-project.git"

  project_name = "Example project"
  project_id   = "example-project"

  providers = {
    google      = google.admin
    google-beta = google-beta.admin
  }
}

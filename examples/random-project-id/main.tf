# Create a GCP project with random id with fixed prefix. This example creates
# project names of the form "some-prefix-[0-9a-f]{8}".

locals {
  project_id_prefix = "some-prefix-"
}

resource "random_id" "project_id" {
  byte_length = 4
  prefix      = local.project_id_prefix
}

module "project" {
  source = "git::ssh://git@gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-project.git"

  project_name = "Example project"
  project_id   = random_id.project_id.hex

  # See the providers.tf file in root-example
  providers = {
    google      = google.admin
    google-beta = google-beta.admin
  }
}

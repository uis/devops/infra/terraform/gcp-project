# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.4.2] - 2023-11-02

### Added

* Added support to publish to GitLab Terraform registry when tagged using semver

## [2.4.1] - 2023-02-13
### Changed
 - Fix tflint warnings.

## [2.4.0] - 2022-03-07
### Added
 - Specify provider versions in version.tf.

## [2.3.0] - 2021-04-14
### Changed
 - Variable `log_retention` for overriding of the default log retention

## [2.2.0] - 2021-02-08
### Changed
 - Project service indexing to be by name instead of by integer.

## [2.1.0] - 2020-09-12
### Added
 - Variable `labels` for specifying project labels.
 - Variable `auto_create_network` for specifying whether to create the
   'default' network automatically"

### Changed
 - Default billing account.
 - Default services to include `serviceusage.googleapis.com`.

## [2.0.0] - 2020-03-31
### Added
 - Support for Terraform v0.12.

### Removed
 - Support for Terraform v0.11 and earlier.

## [1.1.0] - 2019-06-06
### Added
 - Project number as a Terraform output.
 - Examples of usage.
 - Branch (`v0.11`) for this version. Branch name reflects Terraform version
   compatibility.

### Changed
 - Default billing account.

## [1.0.0] - 2019-01-03
### Added
 - Initial implementation supporting Terraform v0.11.
